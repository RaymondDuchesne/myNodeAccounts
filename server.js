var express = require('express'),
  cors = require('cors'),             // Cors will take care of inter domains security issues.
  app = express(),                    // Express for REST routing
  port = process.env.PORT || 3000,
  Sequelize = require('sequelize'),   // Sequilize for DB mapping (ORM tool)
  bodyParser = require('body-parser');

  var swaggerUi = require('swagger-ui-express'),
  swaggerDocument = require('./TestsAndDoc/Swagger/swagger.json');


  app.use(cors());
  app.options('*',cors());            // This is equivalent to applying the CORS authorizations in all method headers

  Sequelize.Promise = global.Promise;

  const sequelize = new Sequelize(
    'postgresql://avesadmin:avesadmin@aves-master.c0azff0quula.us-east-1.rds.amazonaws.com:5432/avesmaster',  
    {
      dialect: 'postgres',
      dialectOptions: {ssl:'Amazon RDS',appName: 'NodeJS_Document_API'},
      pool:{max: 3, min: 0, acquire: 30000, idle: 10000},
    });

  // This export must be placed before activity is created to function correctly
  module.exports =  sequelize;
  
  sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// Very cool!!! if you browse to http://localhost:3000/api-docs/ your doc appears!


var routes = require('./api/routes/todoListRoutes'); //importing route
routes(app); //register the route

app.listen(port);           // READY TO GO after having opened the routing
console.log('NodeJS RESTful API server for Account Service started on: ' + port);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});


