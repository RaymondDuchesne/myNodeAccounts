'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/todoListControllers');

  // Company service Routes
  app.route('/companyservice/companies')
    .get(todoList.list_all_companies);

  // Supports requests like http://localhost:3000/companyservice/company/1
  app.route('/companyservice/company/:Id')
    .get(todoList.read_a_company);

  // We only need a name to create a company
  app.route('/companyservice/createcompany/:Name')
    .post(todoList.create_a_company);

  // Soft delete by ID of the company
  app.route('/companyservice/deletecompany/:Id')
    .delete(todoList.delete_a_company);

  // Update a company name
  app.route('/companyservice/updatecompany/:Id/:Name')
    .put(todoList.update_a_company);

  // Undelete a company - admin function
  app.route('/companyservice/restorecompany/:Id')
    .put(todoList.undelete_a_company);

  // Team service Routes much like companies...

  app.route('/teamservice/teams')
    .get(todoList.list_all_teams);

  app.route('/teamservice/team/:Id')
    .get(todoList.read_a_team);

  app.route('/teamservice/createteam/:Name/:ParentId')
    .post(todoList.create_a_team);

  app.route('/teamservice/deleteteam/:Id')
    .delete(todoList.delete_a_team);

  app.route('/teamservice/updateteam/:Id/:Name?/:ParentId?')
    .put(todoList.update_a_team);

  app.route('/teamservice/undeleteteam/:Id')
    .put(todoList.undelete_a_team);

  // User service Routes

  app.route('/userservice/users')
    .get(todoList.list_all_users);   

  app.route('/userservice/user/:Id')
    .get(todoList.read_a_user);

  app.route('/userservice/usermembership/:Id')
    .get(todoList.read_a_user_and_team);

  app.route('/userservice/deleteuser/:Id')
    .delete(todoList.delete_a_user);

  app.route('/userservice/createuser/:FirstName/:LastName/:Email/:Phone?/:Position?/:CompanyID?/:SubTeamID?/:Location?')
    .post(todoList.create_a_user);

  app.route('/userservice/updateuser/:Id/:FirstName?/:LastName?/:Email?/:Phone?/:Position?/:CompanyID?/:Location?')
    .put(todoList.update_a_user);

  app.route('/userservice/undeleteuser/:Id')
    .put(todoList.undelete_a_user);

  app.route('/userservice/addmembership/:UserId/:SubTeamId')
    .put(todoList.add_membership);

  app.route('/userservice/removemembership/:UserId/:SubTeamId')
    .put(todoList.remove_membership);

  app.route('/userservice/restoremembership/:UserId/:SubTeamId')
    .put(todoList.restore_membership);

};
