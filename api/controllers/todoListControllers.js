'use strict';
var Sequelize = require('sequelize');

var Company = require('../models/todoListModels').Company;
var Team = require('../models/todoListModels').Team;
var User = require('../models/todoListModels').User;
var TeamMembership = require('../models/todoListModels').TeamMembership;

// Export all controller methods related to companies
// THE Crud implementation is pretty elementary so it's a good place for a beginning.

// We have two gets, one for all companies one for a particular ID
exports.list_all_companies = function(req, res) {
  var whereStatement = {};
  if (req.headers.deletedentities == null || req.headers.deletedentities != 'true')
      whereStatement = {deleted : 'false'};
  Company.findAll({where: whereStatement}).then(co => {    
    res.json(co);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.read_a_company = function(req, res) {
  Company.findById(req.params.Id).
  then(co => {
    if (co == null) {
      res.status(400);
      res.json('Entity not found.');
    }
    else res.json(co);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.create_a_company = function(req, res) {
  Company.create({deleted : 'false', company_name : req.params.Name}).
  then(co => {
    res.json(co);
  }).
  catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.delete_a_company = function(req, res) {
  Company.findById(req.params.Id).
  then(co => {
    if (co == null) {
      res.status(400);
      res.json('Entity not found.');
    }
    else {
      co.deleted = true;
      co.save();
      return co;
    }
  }).
  then(co => {
    res.json(co);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.update_a_company = function(req, res) {
  Company.findById(req.params.Id).
  then(co => {
    if (co == null) {
      res.status(400);
      res.json('Entity not found.');
    }
    else {
      co.company_name = req.params.Name;
      co.save();
      return co;
    }
  }).
  then(co => {
    res.json(co);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.undelete_a_company = function(req, res) {
  Company.findById(req.params.Id).
  then(co => {
    if (co == null) {
      res.status(400);
      res.json('Entity not found.');
    }
    else {
      co.deleted = false;
      co.save();
      return co;
    }
  }).
  then(co => {
    res.json(co);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};


// Export all controller methods related to teams
exports.list_all_teams = function(req, res) {
  var whereStatement = {};
  if (req.headers.deletedentities == null || req.headers.deletedentities != 'true')
      whereStatement = {deleted : 'false'};

  Team.findAll({where: whereStatement}).then(te => {
    res.json(te);
  }).catch(function (err) {
      res.status(500);
      res.send(err);
    })
  };


  exports.read_a_team = function(req, res) {
    Team.findById(req.params.Id)
    .then(te => {
      if (te == null) {
        res.status(400);
        res.json('Entity not found.');
      }
      else res.json(te);
    }).catch(function (err) {
      res.status(500);
      res.send(err);
    })
  };

  exports.create_a_team = function(req, res) {
    Team.create({deleted : 'false', name : req.params.Name, parent_team : req.params.ParentId}).
    then(te => {
      res.json(te);
    }).catch(function (err) {
      res.status(500);
      res.send(err);
    })
  };

  exports.delete_a_team = function(req, res) {
    Team.findById(req.params.Id).
    then(te => {
      if (te == null) {
        res.status(400);
        res.json('Entity not found.');
      }
      else {
        te.deleted = true;
        te.save();
        return te;
      }
    }).
    then(te => {
      res.json(te);
    }).catch(function (err) {
      res.status(500);
      res.send(err);
    })
  };
  
  exports.update_a_team = function(req, res) {
    Team.findById(req.params.Id).
    then(te => {
      if (te == null) {
        res.status(400);
        res.json('Entity not found.');
      }
      else
      {  
        if (req.params.Name != null) te.name = req.params.Name;
        if (req.params.ParentId != null) te.parent_team = req.params.ParentId;
        te.save();
        return te;
      }
    }).
    then(te => {
      res.json(te);
    }).catch(function (err) {
      res.status(500);
      res.send(err);
    })
  };

  exports.undelete_a_team = function(req, res) {
    Team.findById(req.params.Id).
    then(te => {
      if (te == null) {
        res.status(400);
        res.json('Entity not found.');
      }
      else {
        te.deleted = false;
        te.save();
        return te;
      }
    }).
    then(te => {
      res.json(te);
    }).catch(function (err) {
      res.status(500);
      res.send(err);
    })
  };

  // Export all controller methods related to users
  
exports.list_all_users = function(req, res) {
  var whereStatement = {};
  if (req.headers.deletedentities == null || req.headers.deletedentities != 'true')
      whereStatement = {deleted : 'false'};
    User.findAll({where: whereStatement},{        
      order: [['last_name'],['first_name']],
  }).then(us => {
      res.json(us);
    }).catch(function (err) {
      res.status(500);
      res.send(err);
    })
  };

  
exports.read_a_user = function(req, res) {
  User.findById(req.params.Id,{        
  }).
  then(us => {
    if (us == null) {
      res.status(400);
      res.json('Entity not found.');
    }
    else res.json(us);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

// Greedy version of user get
exports.read_a_user_and_team = function(req, res) {
  User.findById(req.params.Id,{        

  include: [{model: Team, as: 'teams', 
              attributes: {exclude: ['deleted','parent_team'] } ,  
              through: { attributes: []},  
              include: [{model: Team, as: 'parent', attributes: {exclude: ['deleted','parent_team'] }}]}, 
            {model: Company, attributes: ['id', 'company_name']}],
   attributes: {exclude: ['deleted','companyID']}
  }).
  then(us => {
    if (us == null) {
      res.status(400);
      res.json('Entity not found.');
    }
    else res.json(us);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};


exports.create_a_user = function(req, res) {
      User.create({deleted : 'false', first_name : req.params.FirstName,  last_name : req.params.LastName,
      email : req.params.Email, phone: req.params.Phone, location : req.params.Location,
      position : req.params.Position, company : req.params.CompanyID ,
      team_memberships : [{deleted : 'false', subteam_id : req.params.SubTeamID}]
    },{
      include: [ TeamMembership ]
    }
  ).
  then(us => {
    res.json(us);
  }).
  catch(function (err) {
    res.status(500);
    res.send(err);
  })
};


exports.update_a_user = function(req, res) {
  User.findById(req.params.Id).
  then(us => {
    if (req.params.FirstName != null) us.first_name = req.params.FirstName;
    if (req.params.LastName != null) us.last_name = req.params.LastName;
    if (req.params.Email != null) us.mail = req.params.Email;
    if (req.params.Phone != null) us.phone = req.params.Phone;
    if (req.params.Location != null) us.location = req.params.Location;
    if (req.params.Position != null) us.position = req.params.Position;
    if (req.params.CompanyID != null) us.company = req.params.CompanyID;
    return us.save();
  }).
  then(us => {
    res.json(us);
  }).
  catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.delete_a_user = function(req, res) {
  User.findById(req.params.Id).
  then(us => {
    if (us == null) {
      res.status(400);
      res.json('Entity not found.');
    }
    else {
      us.deleted = 'true';
      us.save();
      return us;
    }
  }).
  then(us => {
    res.status(204);
    res.json(us);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.undelete_a_user = function(req, res) {
  User.findById(req.params.Id).
  then(us => {
    if (us == null) {
      res.status(400);
      res.json('Entity not found.');
    }
    else {
      us.deleted = 'false';
      us.save();
      return us;
    }
  }).
  then(us => {
    res.json(us);
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.remove_membership = function(req, res) {
  TeamMembership.findOne(
    {   
      where: {deleted: false, user_id: req.params.UserId, subteam_id : req.params.SubTeamId}
    }).
    then(m => {
      if (m != null) {
        m.deleted = 'true';
        m.save();
      }
      return m;
    }).
  then((m) => {
    if (m != null)
    {
      res.status(204);
      res.json('Membership deleted.');
    }
    else {
      res.status(400);
      res.json('Membership not found.');
    }
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

exports.add_membership = function(req, res) {
  var team = TeamMembership.build(
    { });
    team.user_id =  req.params.UserId; team.subteam_id = req.params.SubTeamId;
    team.save().
    then(m => {
      res.json(m);
    }).catch(function (err) {
    res.send(err);
  })
};

exports.restore_membership = function(req, res) {
  TeamMembership.findOne(
    {   
      where: {deleted: true, user_id: req.params.UserId, subteam_id : req.params.SubTeamId}
    }).
  then(m => {
    if (m != null) {
      m.deleted = 'false';
      m.save();
    }
    return m;
  }). 
   then(m => {
    if (m != null)
    {
      res.json('Membership restored.');
    }
    else {
      res.status(400);
      res.json('Membership not found.');
    }
  }).catch(function (err) {
    res.status(500);
    res.send(err);
  })
};

