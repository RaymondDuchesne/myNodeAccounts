
'use strict';
var Sequelize = require('sequelize');
const sequelize = require('../../server.js');



const Company = sequelize.define('company', {
    id: { 
      type: Sequelize.INTEGER,
      unique: true,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    company_name: {
      type: Sequelize.STRING
    },          
    deleted: {
      type: Sequelize.BOOLEAN
    }
  },
{ // options
  timestamps: false, freezeTableName: true, tablename:  'company' , schema: 'aves'
});



const User = sequelize.define('user', {
  id: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  first_name: {
    type: Sequelize.STRING
  },          
  last_name: {
    type: Sequelize.STRING
  },    
  email: {
    type: Sequelize.STRING
  },
  phone: {
    type: Sequelize.STRING
  },
  location: {
    type: Sequelize.STRING
  },           
  companyID: {
    type: Sequelize.INTEGER,
    field: "company"
  },    
  position: {
    type: Sequelize.STRING
  },
  deleted: {
    type: Sequelize.BOOLEAN
  }             
}
,
{ // options
  timestamps: false, freezeTableName: true, tablename:  'user' , schema: 'aves'
});


const Team = sequelize.define('team', {
    id: {
      type: Sequelize.INTEGER,
      unique: true,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },      
    parent_team: {
        type: Sequelize.INTEGER,
        allowNull: true
      },      
    deleted: {
      type: Sequelize.BOOLEAN
    }             
},
{ // options
  timestamps: false, freezeTableName: true, tablename:  'team' , schema: 'aves'
});

const TeamMembership = sequelize.define('team_membership',
{
    id: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    subteam_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },      
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },      
    deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: 'false'
    }      
},
{ // options
  timestamps: false, freezeTableName: true, tablename:  'team_membership' , schema: 'aves'
});


User.team_memberships = User.hasMany(TeamMembership,{foreignKey:'user_id',targetKey:'id'});
User.teams = User.belongsToMany(Team,  { through: 'team_membership', foreignKey: 'user_id', otherKey: 'subteam_id'})
User.company = User.belongsTo(Company, {foreignKey: 'companyID'});
Team.parent = Team.belongsTo(Team, {as: 'parent', foreignKey: 'parent_team'})
module.exports = {Company,Team,User,TeamMembership};
